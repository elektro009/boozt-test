import React, { Component } from "react";
import "./App.scss";
import ProductList from "./components/ProductList";

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="app-content">
          <ProductList />
        </div>
      </div>
    );
  }
}

export default App;
