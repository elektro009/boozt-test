import React from "react";
import ProductCard from "./ProductCard";
import { Container } from "react-bootstrap";
import { Row } from "react-bootstrap";
import Pagination from "react-bootstrap/Pagination";
import ProductFilter from "./ProductFilter";
import products from "../products";
class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      prodList: []
    };
    this.products = products;
    this.state.active = 1;
    this.state.itemsPerPage = 20;
    this.state.prodList = this.products.slice(
      this.state.itemsPerPage * (this.state.active - 1),
      this.state.itemsPerPage * (this.state.active - 1) +
        this.state.itemsPerPage
    );

    this.state.last = Math.ceil(this.products.length / this.state.itemsPerPage);
    this.paginationEventNext = this.paginationEventNext.bind(this);
    this.paginationEventPrevious = this.paginationEventPrevious.bind(this);
    this.paginationEventPage = this.paginationEventPage.bind(this);
    this.updateVisibleList = this.updateVisibleList.bind(this);
    this.createPaginationItems = this.createPaginationItems.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
  }
  paginationEventNext() {
    this.updateVisibleList(this.state.active + 1);
  }
  paginationEventPrevious() {
    this.updateVisibleList(this.state.active - 1);
  }
  paginationEventPage(page) {
    this.updateVisibleList(page);
  }
  updateVisibleList(page) {
    let start = this.state.itemsPerPage * (page - 1);
    let end = this.state.itemsPerPage * (page - 1) + this.state.itemsPerPage;
    let list = this.products.slice(start, end);

    this.setState({ active: page });
    this.setState({ prodList: list });
    this.forceUpdate();
  }
  createPaginationItems() {
    let items = [];
    for (let i = 1; i < this.state.last + 1; i++) {
      items.push(
        <Pagination.Item
          key={i}
          active={i === this.state.active}
          onClick={this.paginationEventPage.bind(this, i)}
          className={
            (this.state.active - 4 > i) | (this.state.active + 4 < i)
              ? "d-none"
              : ""
          }
        >
          {i}
        </Pagination.Item>
      );
    }
    return items;
  }
  handleFilter(ascDesc) {
    let data = this.products;
    let sortData = data.sort((a, b) => {
      if (ascDesc > 0) {
        return a["actual_price"] - b["actual_price"];
      }
      return b["actual_price"] - a["actual_price"];
    });
    this.products = sortData;
    this.paginationEventPage(1);
  }
  render() {
    return (
      <div>
        <Container>
          <Row className="text-center h1">Products</Row>
          <Row>
            <ProductFilter
              changeFilter={this.handleFilter}
              filterProp="actual_price"
            />
          </Row>
          <Row>
            {this.state.prodList.map((item, i) => (
              <ProductCard key={i} componentData={item} />
            ))}
          </Row>
          <Row>
            <Pagination className="product-pagination" size="sm">
              <Pagination.Item onClick={this.paginationEventPage.bind(this, 1)}>
                {1}
              </Pagination.Item>
              <Pagination.Prev
                disabled={this.state.active === 1}
                onClick={this.paginationEventPrevious}
              />
              {this.createPaginationItems()}
              <Pagination.Next
                disabled={this.state.active === this.state.last}
                onClick={this.paginationEventNext}
              />
              <Pagination.Item
                onClick={this.paginationEventPage.bind(this, this.state.last)}
              >
                {this.state.last}
              </Pagination.Item>
            </Pagination>
          </Row>
        </Container>
      </div>
    );
  }
}
export default ProductList;
