import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSortAmountUp,
  faSortAmountDown
} from "@fortawesome/free-solid-svg-icons";
class ProductFilter extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
    this.state.ascDesc = 0;
    this.changeFilter = this.changeFilter.bind(this);
  }
  changeFilter(ascDesc) {
    this.props.changeFilter(ascDesc);
    this.setState({ ascDesc: ascDesc });
  }
  render() {
    return (
      <div
        className={
          this.state.ascDesc === 0
            ? "product-filter product-filter__inactive"
            : "product-filter"
        }
      >
        <FontAwesomeIcon
          icon={faSortAmountUp}
          onClick={this.changeFilter.bind(this, -1)}
          className={this.state.ascDesc < 0 ? "d-none" : ""}
        />
        <FontAwesomeIcon
          icon={faSortAmountDown}
          onClick={this.changeFilter.bind(this, 1)}
          className={this.state.ascDesc > 0 ? "d-none" : ""}
        />
      </div>
    );
  }
}
export default ProductFilter;
