import React from "react";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import { Row } from "react-bootstrap";
class ProductCard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }
  render() {
    return (
      <Col className="product-card" md={12}>
        <Card>
          <Row>
            <Col
              xs={12}
              md={3}
              lg={2}
              className="text-center align-middle product-card__img"
            >
              <Card.Img
                className=""
                variant="top"
                src={this.props.componentData.filename}
              />
            </Col>
            <Col xs={12} md={9} lg={10}>
              <Card.Body className="product-card__txt">
                <Card.Title>{this.props.componentData.product_name}</Card.Title>
                <Card.Text>{this.props.componentData.brand_name}</Card.Text>
                <Card.Text className="text-right">
                  {new Intl.NumberFormat("eu", {
                    style: "currency",
                    currency: "EUR",
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2
                  }).format(this.props.componentData.actual_price)}
                </Card.Text>
              </Card.Body>
            </Col>
          </Row>
        </Card>
      </Col>
    );
  }
}
export default ProductCard;
